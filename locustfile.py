from locust import HttpLocust, TaskSet

def randomImage(l):
    l.client.get("breeds/image/random")

class UserBehavior(TaskSet):
    tasks = {randomImage}

class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 5000
    max_wait = 9000 